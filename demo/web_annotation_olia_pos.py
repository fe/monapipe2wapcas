from typing import List

from spacy.tokens import Doc, Span, Token
from tgclients.aggregator import Aggregator

import monapipe.model


def token_based_web_annotations(doc: Doc, source: str) -> List[str]:
    """Export method for spaCy tokens.

    Args:
        doc: spacy doc.
        source: source url.

    Returns:
        list: list of json outputs (str).
    """
    mapping = {
        "ADJ": "http://purl.org/olia/olia.owl#Adjective",
        "ADP": "http://purl.org/olia/olia.owl#Adposition",
        "ADV": "http://purl.org/olia/olia.owl#Adverb",
        "AUX": "http://purl.org/olia/olia.owl#AuxiliaryVerb",
        "CONJ": "http://purl.org/olia/olia.owl#CoordinatingConjunction",
        "DET": "http://purl.org/olia/olia.owl#Determiner",
        "INTJ": "http://purl.org/olia/olia.owl#Interjection",
        "NOUN": "http://purl.org/olia/olia.owl#CommonNoun",
        "NUM": "http://purl.org/olia/olia.owl#Quantifier",
        "PART": "http://purl.org/olia/olia.owl#Particle",
        "PRON": "http://purl.org/olia/olia.owl#Pronoun",
        "PROPN": "http://purl.org/olia/olia.owl#ProperNoun",
        "PUNCT": "http://purl.org/olia/olia.owl#Punctuation",
        "SCONJ": "http://purl.org/olia/olia.owl#SubordinatingConjunction",
        "SYM": "http://purl.org/olia/olia.owl#Symbol",
        "VERB": "http://purl.org/olia/olia.owl#Verb",
        "CCONJ": "CCONJ",
        "SPACE": "SPACE",
        "X": "X",
    }

    jsons = list()
    for token in doc:
        json = """{
                "@context": "http://www.w3.org/ns/anno.jsonld",
                "type": "Annotation",
                "body": {
                    "@id": "%s"
                },
                "target": {
                    "source": "%s",
                    "selector": {
                        "type": "TextPositionSelector",
                        "start": %i,
                        "end": %i
                    }
                }
            } 
            """ % (
            mapping[token.pos_],
            source,
            token.idx,
            token.idx + len(token),
        )
        jsons.append(json)

    return jsons


def upos_for_textgrid_uri(textgrid_uri: str, slice_: slice = slice(0, None) ) -> List[str]:
    base_url = "https://textgridlab.org/1.0/aggregator/text/"

    aggregator = Aggregator()
    text = aggregator.text(textgrid_uri).text
    nlp = monapipe.model.load(disable=["ner"])

    doc = nlp(text[slice_])

    return token_based_web_annotations(doc=doc, source=base_url + textgrid_uri)


if __name__ == "__main__":
    for json in upos_for_textgrid_uri("textgrid:11hnp.0"):
        print(json)
