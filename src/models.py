"""Pydantic models for data validation by FastAPI."""
from typing import Any, Union

from pydantic import BaseModel, ConfigDict, Field


class Annotation(BaseModel):
    """An Annotation object."""

    context: str = Field(alias="@context", default="http://www.w3.org/ns/anno.jsonld")
    id: Union[str, None] = Field(default=None)
    canonical: Union[str, None] = Field(default=None)
    type: str = "Annotation"
    body: Any = None
    target: Any = None  # made optional for testing purposes
    via: str | None = None
    created: Union[str, None] = None
    modified: Union[str, None] = None

    model_config = ConfigDict(
        # Allow an aliased field to be populated by its name as given by the model attribute.
        populate_by_name=True,
        # Example for `openapi.json`.
        json_schema_extra={
            "example": {
                "@context": "http://www.w3.org/ns/anno.jsonld",
                "id": "http://example.org/anno1",
                "type": "Annotation",
                "body": "http://example.org/post1",
                "target": "http://example.com/page1",
            }
        },
    )


class Collection(BaseModel):
    """An Annotation Collection."""

    context: str = Field(alias="@context", default="http://www.w3.org/ns/anno.jsonld")
    id: Union[str, None] = Field(default=None)
    type: Union[str, list[str]] = "AnnotationCollection"
    # a container cannot be modified, see https://www.w3.org/TR/annotation-model/#index-of-json-keys
    # but: "The Collection SHOULD include the modified property with the most
    # recent timestamp of when any of the annotations in the Collection." => LOL
    # this is necessary to have a different ETAG if a later revision is equal to a former
    modified: Union[str, None] = None
    created: Union[str, None] = None  # testing only, must be str only
    # currently unused/not implemented
    label: Union[str, None] = None
    total: int = 0
    # muss gefüllt werden bzw. page muss erstellt werden, wenn die erste
    # annotation hinzugefügt wird!
    first: Union[str, None] = None
    last: Union[str, None] = None

    model_config = ConfigDict(
        # Allow an aliased field to be populated by its name as given by the model attribute.
        populate_by_name=True,
        # Example for `openapi.json`.
        json_schema_extra={
            "example": {
                "@context": "http://www.w3.org/ns/anno.jsonld",
                "id": "http://example.org/collection1",
                "type": "AnnotationCollection",
                "label": "Steampunk Annotations",
                "creator": "http://example.com/publisher",
                "total": 42023,
                "first": "http://example.org/page1",
                "last": "http://example.org/page42",
            }
        },
    )


# gibt es nur als response
class Page(BaseModel):
    """An Annotation Collection’s Page."""

    context: str = Field(alias="@context", default="http://www.w3.org/ns/anno.jsonld")
    id: str
    type: Union[str, list[str]] = "AnnotationPage"
    items: Union[list[str], list[Annotation]]
    next: Union[str, None] = None  # mandatory except for the last page
    prev: Union[str, None] = None  # mandatory except for the first page
    startIndex: int = 0
    partOf: str
