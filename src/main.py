"""App entrypoint."""
import datetime
import hashlib
import logging
import os
import pickle
import uuid
from typing import Annotated, Any, Final

from fastapi import FastAPI, Header, HTTPException, status
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse, Response
from pymongo import MongoClient
from pymongo.collection import Collection
from pymongo.errors import CollectionInvalid

from . import models


PROTOCOL: Final[str] = os.getenv("WAPCAS_PROTOCOL", "http")
HOST: Final[str] = os.getenv("WAPCAS_HOST", "localhost")
PORT: Final[str] = os.getenv("WAPCAS_PORT", "8000")

SERVICE_URL = f"{PROTOCOL}://{HOST}:{PORT}"

# have a look at [structured logging](https://docs.python.org/3/howto/logging-cookbook.html#implementing-structured-logging)
# and https://stackoverflow.com/questions/50144628/python-logging-into-file-as-a-dictionary-or-json
logging.basicConfig(
    # format="%(asctime)s,%(msecs)03d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s",
    datefmt="%Y-%m-%d:%H:%M:%S",
    level=logging.DEBUG,
)
logger = logging.getLogger(__name__)


def isotimestamp():
    """function shortcut alias for getting an iso formatted timestamp"""
    return datetime.datetime.now(datetime.timezone.utc).isoformat()


def create_etag(input: Any) -> str:
    """Creates a hexadecimal digest of an input-object’s SHA256 hash to be used as an ETAG.

    Args:
        input (str): Input data.

    Returns:
        str: ETAG
    """
    # TODO: refactor to use json.dumps
    return hashlib.sha256(pickle.dumps(input), usedforsecurity=False).hexdigest()


JSON_LD_MIME_TYPE: Final[str] = "application/ld+json"
WEB_ANNOTATION_MIME_TYPE: Final[str] = (
    JSON_LD_MIME_TYPE + "; profile='http://www.w3.org/ns/anno.jsonld'"
)
CONTAINER_LINK_TYPE: Final[
    str
] = '<http://www.w3.org/ns/ldp#BasicContainer>; rel="type"'
CONTAINER_LINK_CONSTRAINT: Final[
    str
] = '<http://www.w3.org/TR/annotation-protocol/>; rel="http://www.w3.org/ns/ldp#constrainedBy"'
CONTAINER_PREFER_MINIMAL: Final[
    str
] = 'return=representation;include="http://www.w3.org/ns/ldp#PreferMinimalContainer"'
CONTAINER_PREFER_IRIS: Final[
    str
] = 'return=representation;include="http://www.w3.org/ns/oa#PreferContainedIRIs"'
CONTAINER_PREFER_DESCRIPTIONS: Final[
    str
] = 'return=representation;include="http://www.w3.org/ns/oa#PreferContainedDescriptions"'

ANNOTATION_ALLOW: Final[str] = "OPTIONS, GET, HEAD, POST, PUT, DELETE"  # everything
COLLECTION_ALLOW: Final[str] = "OPTIONS, GET, HEAD, POST, DELETE"  # no update
PAGE_ALLOW: Final[str] = "OPTIONS, GET, HEAD"  # read-only
# response constants (headers)
RESPONSE_CONTENT_TYPE = WEB_ANNOTATION_MIME_TYPE
RESPONSE_ACCEPT_POST = WEB_ANNOTATION_MIME_TYPE


# JSON-LD response class
class JSONLDResponse(JSONResponse):
    media_type = RESPONSE_CONTENT_TYPE


client: MongoClient = MongoClient("mongodb://root:example@mongo:27017/")
db = client.wapcas
# think about organizing the documents in three db-collections: collections (`__collections__`), pages (`__pages__`), and annotations/named after collection(?why should that be necessary?) to allow easier projections/views and validation.
try:
    db.create_collection("__collections__")
    db.create_collection("__pages__")
except CollectionInvalid:
    # CollectionInvalid is raised when collections already exist
    pass

COLLECTIONS: Final[Collection] = db["__collections__"]
PAGES: Final[Collection] = db["__pages__"]


# create view test; always drop and recreate during development
db.drop_collection("__collections_with_embedded_pages__")
try:
    db.create_collection(
        # this is the view for "PREFER_IRIS"
        "__collections_with_embedded_pages__",
        viewOn="__collections__",
        # check_exists=False,
        pipeline=[
            {
                "$lookup": {
                    "from": "__pages__",
                    "localField": "first",
                    "foreignField": "id",
                    "as": "first",
                }
            },
            # destruct array
            {"$unwind": {"path": "$first"}},
            # remove _id, @context, partOf
            {
                "$project": {
                    "_id": False,
                    "first._id": False,
                    "first.@context": False,
                    "first.partOf": False,
                }
            },
        ],
    )
except:
    pass
PAGES_VIEW: Final[Collection] = db["__collections_with_embedded_pages__"]


# create view test; always drop and recreate during development
db.drop_collection("__collections_with_embedded_annotations__")
try:
    db.create_collection(
        # this is an example/testing view for "PREFER_DESCRIPTIONS" based on the "test" collection
        "__collections_with_embedded_annotations__",
        viewOn="__collections_with_embedded_pages__",
        # check_exists=False,
        pipeline=[
            {
                "$lookup": {
                    "from": "test",  # this is suppsed to be dynamic; or all annotations need to be in the same db-collection; think about it!
                    "localField": "first.items",
                    "foreignField": "id",
                    "as": "first.items",
                }
            },
            # remove _id, @context, partOf
            {
                "$project": {
                    "first.items._id": False,
                    "first.items.@context": False,
                }
            },
        ],
    )
except:
    pass

logger.warning(
    f"FOUND ONE? {db['__collections_with_embedded_annotations__'].find_one()}"
)


app = FastAPI()

# CORS config for testing with web-platform-tests
app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        "http://localhost",
        "http://localhost:8000",
    ],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


# TODO: split code according to https://fastapi.tiangolo.com/tutorial/bigger-applications/


# ---------------------------------
# Annotation Collections CR(U)D API
# ---------------------------------
@app.post(
    "/",
    responses={
        status.HTTP_201_CREATED: {},
        status.HTTP_409_CONFLICT: {},
        status.HTTP_503_SERVICE_UNAVAILABLE: {},
    },
    response_class=JSONLDResponse,
    response_model=models.Collection,
    response_model_exclude_none=True,
    summary="Create a Web Annotation Collection.",
    response_description="The created Annotation Collection.",
    tags=["collections"],
)
def create_collection(
    response: JSONResponse,
    slug: str | None = Header(default=None),
):
    """
    Create a Web Annotation Collection with an optional
    `slug` for the Collection ID and return it.
    """

    new_id = slug or str(uuid.uuid4())

    COLLECTION_ID: Final[str] = f"{SERVICE_URL}/{new_id}/"

    collection = COLLECTIONS.find_one({"id": COLLECTION_ID})
    if collection:
        raise CollectionExists(new_id, COLLECTION_ID)

    timestamp: Final[str] = isotimestamp()

    input_collection = models.Collection(
        id=COLLECTION_ID,
        created=timestamp,
    )

    _id = COLLECTIONS.insert_one(input_collection.model_dump(by_alias=True)).inserted_id

    output_collection = COLLECTIONS.find_one({"_id": _id})

    etag = create_etag(output_collection)

    response.headers["Allow"] = COLLECTION_ALLOW
    response.headers["Content-Type"] = RESPONSE_CONTENT_TYPE
    # preserve duplicate "Link" header by using append
    response.headers.append("Link", CONTAINER_LINK_TYPE)
    response.headers.append("Link", CONTAINER_LINK_CONSTRAINT)
    response.headers["Etag"] = etag
    response.headers["Location"] = COLLECTION_ID
    response.status_code = status.HTTP_201_CREATED
    return output_collection


@app.get(
    "/{collection_slug}/",
    responses={
        status.HTTP_200_OK: {},
        status.HTTP_404_NOT_FOUND: {},
    },
    response_class=JSONLDResponse,
    response_model=models.Collection,
    response_model_exclude_none=True,
    summary="Retrieve an Annotation Collection.",
    response_description="The requested Annotation Collection.",
    tags=["collections"],
)
@app.get(
    "/{collection_slug}",
    responses={
        status.HTTP_200_OK: {},
        status.HTTP_404_NOT_FOUND: {},
    },
    response_class=JSONLDResponse,
    response_model=models.Collection,
    response_model_exclude_none=True,
    include_in_schema=False,
)
def retrieve_collection(
    collection_slug: str,
    response: JSONLDResponse,
    page: int = 0,
    prefer: Annotated[str | None, Header()] = None,
) -> Any:
    """Retrieve an Annotation Collection (`collection_slug`)."""

    COLLECTION_ID: Final[str] = f"{SERVICE_URL}/{collection_slug}/"

    # prepare here for serving the 3 different views with a switch/case on the prefer header
    # TODO: modify Annotation and Page model for embedding (removed @context/partOf, that is required otherwise) to allow for data validation on response



    collection_document = COLLECTIONS.find_one({"id": COLLECTION_ID})

    if not collection_document:
        raise CollectionNotFoundException(collection_slug, COLLECTION_ID)
    etag = create_etag(collection_document)

    response.headers["Allow"] = COLLECTION_ALLOW
    response.headers["Content-Type"] = RESPONSE_CONTENT_TYPE
    response.headers["Content-Location"] = COLLECTION_ID
    response.headers["Etag"] = etag
    # preserve duplicate "Link" header by using append
    response.headers.append("Link", CONTAINER_LINK_TYPE)
    response.headers.append("Link", CONTAINER_LINK_CONSTRAINT)
    response.headers["Accept-Post"] = RESPONSE_ACCEPT_POST
    response.headers["Vary"] = "Accept"

    response.status_code = status.HTTP_200_OK

    # remember: if you return a response directly, it is not validated against a model!
    return collection_document


# solution for 204 problem should be https://github.com/tiangolo/fastapi/issues/717#issuecomment-583826657
# but it does not seem to work here?
@app.delete(
    "/{collection_slug}/",
    responses={
        status.HTTP_204_NO_CONTENT: {"model": None},
        status.HTTP_404_NOT_FOUND: {},
    },
    response_class=Response,
    # status_code=status.HTTP_204_NO_CONTENT,
    summary="Delete an Annotation Collection.",
    response_description="Empty response.",
    tags=["collections"],
)
@app.delete(
    "/{collection_slug}",
    responses={
        status.HTTP_204_NO_CONTENT: {"model": None},
        status.HTTP_404_NOT_FOUND: {},
    },
    response_class=Response,
    include_in_schema=False,
)
def delete_collection(
    collection_slug: str,
    # response: Response,
    if_match: Annotated[str | None, Header()] = None,
) -> Any:
    """Delete an Annotation Collection (`collection_id`)."""

    COLLECTION_ID: str = f"{SERVICE_URL}/{collection_slug}/"
    collection = COLLECTIONS.find_one({"id": COLLECTION_ID})
    if not collection:
        raise CollectionNotFoundException(collection_slug, COLLECTION_ID)
    etag = create_etag(collection)
    if if_match == etag:
        # delete collection metadata document
        COLLECTIONS.delete_one({"id": COLLECTION_ID})
        # drop database collection with all annotations
        db.drop_collection(collection_slug)
        # response.status_code = status.HTTP_204_NO_CONTENT
        return Response(status_code=status.HTTP_204_NO_CONTENT)

    # TODO: handle no-match and/or allow delete empty collection without match


@app.options("/{collection_slug}/", tags=["collections"])
@app.options("/{collection_slug}", include_in_schema=False)
def options_collection(response: Response):
    response.headers["Allow"] = COLLECTION_ALLOW


@app.head("/{collection_slug}/", tags=["collections"])
@app.head("/{collection_slug}", include_in_schema=False)
def head_collection(response: Response):
    response.headers["Allow"] = COLLECTION_ALLOW
    response.headers["Content-Type"] = RESPONSE_CONTENT_TYPE
    # preserve duplicate "Link" header by using append
    response.headers.append("Link", CONTAINER_LINK_TYPE)
    response.headers.append("Link", CONTAINER_LINK_CONSTRAINT)
    response.headers["Accept-Post"] = RESPONSE_ACCEPT_POST


# ------------------------
# Web Annotations CRUD API
# ------------------------
# TODO: document responses (e.g. 201)
@app.post(
    "/{collection_slug}/",
    responses={
        status.HTTP_201_CREATED: {},
        status.HTTP_404_NOT_FOUND: {},
        status.HTTP_409_CONFLICT: {},
    },
    response_class=JSONLDResponse,
    response_model=models.Annotation,
    response_model_exclude_none=True,
    summary="Create a Web Annotation.",
    tags=["annotations"],
)
@app.post(
    "/{collection_slug}",
    responses={
        status.HTTP_201_CREATED: {},
        status.HTTP_404_NOT_FOUND: {},
        status.HTTP_409_CONFLICT: {},
    },
    response_class=JSONLDResponse,
    response_model=models.Annotation,
    response_model_exclude_none=True,
    include_in_schema=False,
)
def create_annotation(
    collection_slug: str,
    annotation: models.Annotation,
    response: JSONResponse,
    slug: str | None = Header(default=None),
):
    """
    Create a Web Annotation in a Collection (`collection_slug`) with an optional
    `slug` for the Annotation ID and return it.
    """

    COLLECTION_ID: Final[str] = f"{SERVICE_URL}/{collection_slug}/"
    collection_document = COLLECTIONS.find_one({"id": COLLECTION_ID})
    if not collection_document:
        raise CollectionNotFoundException(collection_slug, COLLECTION_ID)

    new_id = slug or str(uuid.uuid4())
    ANNOTATION_ID: Final[str] = COLLECTION_ID + new_id
    annotation_exists = db[collection_slug].find_one({"id": ANNOTATION_ID})
    if annotation_exists:
        raise HTTPException(
            status_code=409, detail=f"Annotation '{new_id}' ({ANNOTATION_ID}) exists."
        )

    timestamp: str = isotimestamp()

    if annotation.id:
        annotation.via = annotation.id

    annotation.id = ANNOTATION_ID
    annotation.created = timestamp
    annotation.modified = timestamp

    db_collection = db[collection_slug]
    _id = db_collection.insert_one(annotation.model_dump(by_alias=True)).inserted_id
    output_annotation = db_collection.find_one({"_id": _id})

    # TODO: implement logic for page creation when first annotation arrives
    # check `if not collecion.first` create page
    # also check out On-demand materialized views in mongodb vs standard views
    if not collection_document["first"]:
        create_first_page(annotation.id, collection_slug)
    else:
        update_page(annotation.id, collection_document["first"])

    # update "modified" and "total" in the collection document
    COLLECTIONS.update_one({"id": COLLECTION_ID}, {"$set": {"modified": timestamp}})
    COLLECTIONS.update_one({"id": COLLECTION_ID}, {"$inc": {"total": 1}})

    etag = create_etag(output_annotation)

    response.headers["Allow"] = ANNOTATION_ALLOW
    response.headers["Content-Type"] = RESPONSE_CONTENT_TYPE
    response.headers["Etag"] = etag
    response.headers["Location"] = ANNOTATION_ID
    response.status_code = status.HTTP_201_CREATED
    return output_annotation


@app.get(
    "/{collection_slug}/{annotation_slug}/",
    responses={
        status.HTTP_200_OK: {},
        status.HTTP_404_NOT_FOUND: {},
    },
    response_class=JSONLDResponse,
    response_model=models.Annotation,
    response_model_exclude_none=True,
    summary="Retrieve a Web Annotation.",
    tags=["annotations"],
)
@app.get(
    "/{collection_slug}/{annotation_slug}",
    responses={
        status.HTTP_200_OK: {},
        status.HTTP_404_NOT_FOUND: {},
    },
    response_class=JSONLDResponse,
    response_model=models.Annotation,
    response_model_exclude_none=True,
    include_in_schema=False,
)
def retrieve_annotation(
    collection_slug: str, annotation_slug: str, response: JSONResponse
) -> Any:
    """Retrieve an Annotation (`annotation_slug`) from a Collection (`annotation_slug`)."""

    COLLECTION_ID: Final[str] = f"{SERVICE_URL}/{collection_slug}/"
    collection = db[collection_slug].find_one({"id": COLLECTION_ID})
    if not collection:
        raise CollectionNotFoundException(collection_slug, COLLECTION_ID)

    ANNOTATION_ID: Final[str] = COLLECTION_ID + annotation_slug
    annotation = db[collection_slug].find_one({"id": ANNOTATION_ID})
    if not annotation:
        raise AnnotationNotFoundException(annotation_slug, ANNOTATION_ID)

    etag = create_etag(annotation)

    response.headers["Allow"] = ANNOTATION_ALLOW
    response.headers["Content-Type"] = RESPONSE_CONTENT_TYPE
    response.headers["Etag"] = etag
    response.headers["Link"] = '<http://www.w3.org/ns/ldp#Resource>; rel="type"'
    response.headers["Vary"] = "Accept"
    response.status_code = status.HTTP_200_OK

    return annotation


@app.put(
    "/{collection_slug}/{annotation_slug}/",
    responses={
        status.HTTP_201_CREATED: {},
        status.HTTP_404_NOT_FOUND: {},
        status.HTTP_409_CONFLICT: {},
    },
    response_class=JSONLDResponse,
    response_model=models.Annotation,
    response_model_exclude_none=True,
    summary="Replace a Web Annotation.",
    tags=["annotations"],
)
@app.put(
    "/{collection_slug}/{annotation_slug}",
    responses={
        status.HTTP_200_OK: {},
        status.HTTP_404_NOT_FOUND: {},
        status.HTTP_409_CONFLICT: {},
    },
    response_class=JSONLDResponse,
    response_model=models.Annotation,
    response_model_exclude_none=True,
    include_in_schema=False,
)
def replace_annotation(
    collection_slug: str,
    annotation_slug: str,
    annotation: models.Annotation,
    response: JSONResponse,
    if_match: Annotated[str | None, Header()] = None,
):
    """
    Replace an Annotation (`annotation_slug`) in a Collection (`collection_slug`).
    """

    COLLECTION_ID: Final[str] = f"{SERVICE_URL}/{collection_slug}/"
    collection = COLLECTIONS.find_one({"id": COLLECTION_ID})
    if not collection:
        raise CollectionNotFoundException(collection_slug, COLLECTION_ID)

    ANNOTATION_ID: Final[str] = COLLECTION_ID + annotation_slug
    db_collection = db[collection_slug]
    annotation_exists = db_collection.find_one({"id": ANNOTATION_ID})
    if not annotation_exists:
        raise AnnotationNotFoundException(annotation_slug, ANNOTATION_ID)

    timestamp: str = isotimestamp()

    if annotation.id:
        annotation.via = annotation.id
        annotation.id = ANNOTATION_ID

    annotation.created = timestamp
    annotation.modified = timestamp
    logger.warning(annotation)

    result = db_collection.replace_one(
        {"id": ANNOTATION_ID}, annotation.model_dump(by_alias=True)
    )
    logger.warning(result)

    output_annotation = db_collection.find_one(
        {"id": ANNOTATION_ID}
    )  # think about refactoring to use _id which is certainly the fastest lookup
    etag = create_etag(output_annotation)

    response.headers["Allow"] = ANNOTATION_ALLOW
    response.headers["Content-Type"] = RESPONSE_CONTENT_TYPE
    response.headers["Etag"] = etag
    response.headers["Location"] = ANNOTATION_ID

    return output_annotation


@app.delete(
    "/{collection_slug}/{annotation_slug}/",
    responses={
        status.HTTP_204_NO_CONTENT: {},
        status.HTTP_404_NOT_FOUND: {},
    },
    summary="Delete a Web Annotation.",
    tags=["annotations"],
)
@app.delete(
    "/{collection_slug}/{annotation_slug}",
    responses={
        status.HTTP_204_NO_CONTENT: {},
        status.HTTP_404_NOT_FOUND: {},
    },
    include_in_schema=False,
)
def delete_annotation(
    collection_slug: str, annotation_slug: str, response: JSONResponse
):
    """
    Delete an Annotation (`annotation_slug`) in a Collection (`collection_slug`).
    """

    COLLECTION_ID: Final[str] = f"{SERVICE_URL}/{collection_slug}/"
    collection = COLLECTIONS.find_one({"id": COLLECTION_ID})
    if not collection:
        raise CollectionNotFoundException(collection_slug, COLLECTION_ID)

    ANNOTATION_ID: Final[str] = COLLECTION_ID + annotation_slug
    db_collection: Final[Collection] = db[collection_slug]
    annotation = db_collection.find_one({"id": ANNOTATION_ID})
    if not annotation:
        raise AnnotationNotFoundException(annotation_slug, ANNOTATION_ID)

    db_collection.delete_one({"id": ANNOTATION_ID})

    response.status_code = status.HTTP_204_NO_CONTENT


@app.options(
    "/{collection_slug}/{annotation_slug}/",
    tags=["annotations"],
)
@app.options("/{collection_slug}/{annotation_slug}", include_in_schema=False)
def options_annotation(response: Response):
    response.headers["Allow"] = ANNOTATION_ALLOW


@app.head(
    "/{collection_slug}/{annotation_slug}/",
    tags=["annotations"],
)
@app.head("/{collection_slug}/{annotation_slug}", include_in_schema=False)
def head_annotation(response: Response):
    response.headers["Allow"] = ANNOTATION_ALLOW
    response.headers["Content-Type"] = RESPONSE_CONTENT_TYPE
    # TODO: For HEAD and GET requests, the response MUST have an ETag header with an entity reference value that implements the notion of entity tags from HTTP [rfc7232].
    response.headers["Link"] = '<http://www.w3.org/ns/ldp#Resource>; rel="type"'


def create_first_page(item: str, collection_slug: str) -> str:
    COLLECTION_ID: Final[str] = f"{SERVICE_URL}/{collection_slug}/"
    page_id: Final[str] = COLLECTION_ID + "?page=0"
    page = models.Page(
        id=page_id,
        items=[item],
        partOf=COLLECTION_ID,
        startIndex=0,
    )
    PAGES.insert_one(page.model_dump(by_alias=True))
    # now update collection document "first" and "last" field
    COLLECTIONS.update_one(
        {"id": COLLECTION_ID}, {"$set": {"first": page_id, "last": page_id}}
    )

    # create the embedded annotations view here!

    return page_id


def update_page(annotation_id: str, page_id: str):
    page = PAGES.find_one({"id": page_id})
    items: list = page["items"]
    items.append(annotation_id)
    PAGES.update_one({"id": page_id}, {"$set": {"items": items}})


class CollectionNotFoundException(HTTPException):
    """Raises HTTP 404."""

    def __init__(self, collection_slug: str, collection_id: str):
        super().__init__(
            status_code=404,
            detail=f"Collection '{collection_slug}' ({collection_id}) does not exist.",
            headers={"Allow": COLLECTION_ALLOW},
        )


class AnnotationNotFoundException(HTTPException):
    """Raises HTTP 404."""

    def __init__(self, annotation_slug: str, annotation_id: str):
        super().__init__(
            status_code=404,
            detail=f"Annotation '{annotation_slug}' ({annotation_id}) does not exist.",
            headers={"Allow": ANNOTATION_ALLOW},
        )


class CollectionExists(HTTPException):
    """Raises HTTP 409."""

    def __init__(self, collection_slug: str, collection_id: str):
        super().__init__(
            status_code=409,
            detail=f"Collection '{collection_slug}' ({collection_id}) exists.",
            headers={"Allow": COLLECTION_ALLOW},
        )
