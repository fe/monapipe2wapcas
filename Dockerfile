FROM python:3.12-slim
WORKDIR /
COPY ./requirements.txt /requirements.txt
RUN pip install -U pip \
    && pip install --no-cache-dir --upgrade -r /requirements.txt
CMD ["uvicorn", "src.main:app", "--host", "0.0.0.0", "--port", "8000", "--reload", "--log-level", "debug"]
