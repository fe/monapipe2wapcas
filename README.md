# monapipe and wapcas demonstrator

fill your wapcas with annotations from monapipe - code in jupyter lab


## Run

we have prepared (large) docker images which already contain libs and models,
get them pre-built from our registry with

```sh
docker-compose up
```

if you need to update the images from the registry you can pull them all with

```sh
docker-compose pull
```

**jupyter:**

Jupyter lab is running from http://localhost:8888 - but check your console output for the link containing the token.

You will find the [fill_and_query.ipynb](./demo/fill_and_query.ipynb) in the demo dir in the lab.

**wapcas:**

The API is served from `http://localhost:8000/`, Swagger UI is available at [http://localhost:8000/docs](http://localhost:8000/docs).

Before any operation, create a repository named "merged" with the RDF4J workbench or other means.

**rdf4j**

RDF4J workbench is available at [http://localhost:8080/rdf4j-workbench](http://localhost:8080/rdf4j-workbench).

## Development

if you make changes to the dockerfiles or the code and want to build and run the containers yourself yoo can do with

```sh
docker-compose -f docker-compose-dev.yml up --build
```

